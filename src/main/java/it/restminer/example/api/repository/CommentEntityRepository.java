package it.restminer.example.api.repository;

import it.restminer.example.api.entity.CommentEntity;
import it.restminer.example.api.entity.PostEntity;
import java.lang.Long;
import java.lang.String;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("commentEntityRepository")
public interface CommentEntityRepository extends JpaRepository<CommentEntity, Long>, JpaSpecificationExecutor<CommentEntity> {
  List<CommentEntity> findByCommentId(Long commentId);

  Page<CommentEntity> findByCommentId(Long commentId, Pageable page);

  List<CommentEntity> findByCommentIdIn(Collection<Long> commentIds);

  List<CommentEntity> findByCommentIdNotIn(Collection<Long> commentIds);

  List<CommentEntity> findByCommentIdNull();

  List<CommentEntity> findByCommentIdNotNull();

  List<CommentEntity> findByCommentIdBefore(Long commentId);

  List<CommentEntity> findByCommentIdAfter(Long commentId);

  List<CommentEntity> findByCommentIdGreaterThanEqual(Long commentId);

  List<CommentEntity> findByCommentIdGreaterThan(Long commentId);

  List<CommentEntity> findByCommentIdLessThanEqual(Long commentId);

  List<CommentEntity> findByCommentIdLessThan(Long commentId);

  List<CommentEntity> findByCommentIdBetween(Long commentIdFrom, Long commentIdTo);

  List<CommentEntity> findByPostEntity(PostEntity postEntity);

  Page<CommentEntity> findByPostEntity(PostEntity postEntity, Pageable page);

  List<CommentEntity> findByPostEntityIn(Collection<PostEntity> postEntitys);

  List<CommentEntity> findByPostEntityNotIn(Collection<PostEntity> postEntitys);

  List<CommentEntity> findByPostEntityNull();

  List<CommentEntity> findByPostEntityNotNull();

  List<CommentEntity> findByPostEntityBefore(PostEntity postEntity);

  List<CommentEntity> findByPostEntityAfter(PostEntity postEntity);

  List<CommentEntity> findByPostEntityGreaterThanEqual(PostEntity postEntity);

  List<CommentEntity> findByPostEntityGreaterThan(PostEntity postEntity);

  List<CommentEntity> findByPostEntityLessThanEqual(PostEntity postEntity);

  List<CommentEntity> findByPostEntityLessThan(PostEntity postEntity);

  List<CommentEntity> findByPostEntityBetween(PostEntity postEntityFrom, PostEntity postEntityTo);

  List<CommentEntity> findByCommentText(String commentText);

  Page<CommentEntity> findByCommentText(String commentText, Pageable page);

  List<CommentEntity> findByCommentTextIgnoreCase(String commentText);

  Page<CommentEntity> findByCommentTextIgnoreCase(String commentText, Pageable page);

  List<CommentEntity> findByCommentTextContaining(String commentText);

  Page<CommentEntity> findByCommentTextContaining(String commentText, Pageable page);

  List<CommentEntity> findByCommentTextEndingWith(String commentText);

  Page<CommentEntity> findByCommentTextEndingWith(String commentText, Pageable page);

  List<CommentEntity> findByCommentTextStartingWith(String commentText);

  Page<CommentEntity> findByCommentTextStartingWith(String commentText, Pageable page);

  List<CommentEntity> findByCommentTextNotLike(String commentText);

  Page<CommentEntity> findByCommentTextNotLike(String commentText, Pageable page);

  List<CommentEntity> findByCommentTextLike(String commentText);

  Page<CommentEntity> findByCommentTextLike(String commentText, Pageable page);

  List<CommentEntity> findByCommentTextIn(Collection<String> commentTexts);

  List<CommentEntity> findByCommentTextNotIn(Collection<String> commentTexts);

  List<CommentEntity> findByCommentTextNull();

  List<CommentEntity> findByCommentTextNotNull();

  List<CommentEntity> findByCommentTextBefore(String commentText);

  List<CommentEntity> findByCommentTextAfter(String commentText);

  List<CommentEntity> findByCommentTextGreaterThanEqual(String commentText);

  List<CommentEntity> findByCommentTextGreaterThan(String commentText);

  List<CommentEntity> findByCommentTextLessThanEqual(String commentText);

  List<CommentEntity> findByCommentTextLessThan(String commentText);

  List<CommentEntity> findByCommentAuthor(String commentAuthor);

  Page<CommentEntity> findByCommentAuthor(String commentAuthor, Pageable page);

  List<CommentEntity> findByCommentAuthorIgnoreCase(String commentAuthor);

  Page<CommentEntity> findByCommentAuthorIgnoreCase(String commentAuthor, Pageable page);

  List<CommentEntity> findByCommentAuthorContaining(String commentAuthor);

  Page<CommentEntity> findByCommentAuthorContaining(String commentAuthor, Pageable page);

  List<CommentEntity> findByCommentAuthorEndingWith(String commentAuthor);

  Page<CommentEntity> findByCommentAuthorEndingWith(String commentAuthor, Pageable page);

  List<CommentEntity> findByCommentAuthorStartingWith(String commentAuthor);

  Page<CommentEntity> findByCommentAuthorStartingWith(String commentAuthor, Pageable page);

  List<CommentEntity> findByCommentAuthorNotLike(String commentAuthor);

  Page<CommentEntity> findByCommentAuthorNotLike(String commentAuthor, Pageable page);

  List<CommentEntity> findByCommentAuthorLike(String commentAuthor);

  Page<CommentEntity> findByCommentAuthorLike(String commentAuthor, Pageable page);

  List<CommentEntity> findByCommentAuthorIn(Collection<String> commentAuthors);

  List<CommentEntity> findByCommentAuthorNotIn(Collection<String> commentAuthors);

  List<CommentEntity> findByCommentAuthorNull();

  List<CommentEntity> findByCommentAuthorNotNull();

  List<CommentEntity> findByCommentAuthorBefore(String commentAuthor);

  List<CommentEntity> findByCommentAuthorAfter(String commentAuthor);

  List<CommentEntity> findByCommentAuthorGreaterThanEqual(String commentAuthor);

  List<CommentEntity> findByCommentAuthorGreaterThan(String commentAuthor);

  List<CommentEntity> findByCommentAuthorLessThanEqual(String commentAuthor);

  List<CommentEntity> findByCommentAuthorLessThan(String commentAuthor);

  List<CommentEntity> findByCommentDate(Date commentDate);

  Page<CommentEntity> findByCommentDate(Date commentDate, Pageable page);

  List<CommentEntity> findByCommentDateIn(Collection<Date> commentDates);

  List<CommentEntity> findByCommentDateNotIn(Collection<Date> commentDates);

  List<CommentEntity> findByCommentDateNull();

  List<CommentEntity> findByCommentDateNotNull();

  List<CommentEntity> findByCommentDateBefore(Date commentDate);

  List<CommentEntity> findByCommentDateAfter(Date commentDate);

  List<CommentEntity> findByCommentDateGreaterThanEqual(Date commentDate);

  List<CommentEntity> findByCommentDateGreaterThan(Date commentDate);

  List<CommentEntity> findByCommentDateLessThanEqual(Date commentDate);

  List<CommentEntity> findByCommentDateLessThan(Date commentDate);

  List<CommentEntity> findByCommentDateBetween(Date commentDateFrom, Date commentDateTo);
}
