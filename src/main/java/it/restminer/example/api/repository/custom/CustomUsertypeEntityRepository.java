package it.restminer.example.api.repository.custom;

import it.restminer.example.api.repository.UsertypeEntityRepository;
import org.springframework.stereotype.Repository;

@Repository("customUsertypeEntityRepository")
public interface CustomUsertypeEntityRepository extends UsertypeEntityRepository {
}
