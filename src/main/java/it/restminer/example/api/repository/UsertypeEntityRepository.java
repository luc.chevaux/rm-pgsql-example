package it.restminer.example.api.repository;

import it.restminer.example.api.entity.UsertypeEntity;
import java.lang.Long;
import java.lang.String;
import java.util.Collection;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("usertypeEntityRepository")
public interface UsertypeEntityRepository extends JpaRepository<UsertypeEntity, Long>, JpaSpecificationExecutor<UsertypeEntity> {
  List<UsertypeEntity> findByUsertypeId(Long usertypeId);

  Page<UsertypeEntity> findByUsertypeId(Long usertypeId, Pageable page);

  List<UsertypeEntity> findByUsertypeIdIn(Collection<Long> usertypeIds);

  List<UsertypeEntity> findByUsertypeIdNotIn(Collection<Long> usertypeIds);

  List<UsertypeEntity> findByUsertypeIdNull();

  List<UsertypeEntity> findByUsertypeIdNotNull();

  List<UsertypeEntity> findByUsertypeIdBefore(Long usertypeId);

  List<UsertypeEntity> findByUsertypeIdAfter(Long usertypeId);

  List<UsertypeEntity> findByUsertypeIdGreaterThanEqual(Long usertypeId);

  List<UsertypeEntity> findByUsertypeIdGreaterThan(Long usertypeId);

  List<UsertypeEntity> findByUsertypeIdLessThanEqual(Long usertypeId);

  List<UsertypeEntity> findByUsertypeIdLessThan(Long usertypeId);

  List<UsertypeEntity> findByUsertypeIdBetween(Long usertypeIdFrom, Long usertypeIdTo);

  List<UsertypeEntity> findByUsertypeName(String usertypeName);

  Page<UsertypeEntity> findByUsertypeName(String usertypeName, Pageable page);

  List<UsertypeEntity> findByUsertypeNameIgnoreCase(String usertypeName);

  Page<UsertypeEntity> findByUsertypeNameIgnoreCase(String usertypeName, Pageable page);

  List<UsertypeEntity> findByUsertypeNameContaining(String usertypeName);

  Page<UsertypeEntity> findByUsertypeNameContaining(String usertypeName, Pageable page);

  List<UsertypeEntity> findByUsertypeNameEndingWith(String usertypeName);

  Page<UsertypeEntity> findByUsertypeNameEndingWith(String usertypeName, Pageable page);

  List<UsertypeEntity> findByUsertypeNameStartingWith(String usertypeName);

  Page<UsertypeEntity> findByUsertypeNameStartingWith(String usertypeName, Pageable page);

  List<UsertypeEntity> findByUsertypeNameNotLike(String usertypeName);

  Page<UsertypeEntity> findByUsertypeNameNotLike(String usertypeName, Pageable page);

  List<UsertypeEntity> findByUsertypeNameLike(String usertypeName);

  Page<UsertypeEntity> findByUsertypeNameLike(String usertypeName, Pageable page);

  List<UsertypeEntity> findByUsertypeNameIn(Collection<String> usertypeNames);

  List<UsertypeEntity> findByUsertypeNameNotIn(Collection<String> usertypeNames);

  List<UsertypeEntity> findByUsertypeNameNull();

  List<UsertypeEntity> findByUsertypeNameNotNull();

  List<UsertypeEntity> findByUsertypeNameBefore(String usertypeName);

  List<UsertypeEntity> findByUsertypeNameAfter(String usertypeName);

  List<UsertypeEntity> findByUsertypeNameGreaterThanEqual(String usertypeName);

  List<UsertypeEntity> findByUsertypeNameGreaterThan(String usertypeName);

  List<UsertypeEntity> findByUsertypeNameLessThanEqual(String usertypeName);

  List<UsertypeEntity> findByUsertypeNameLessThan(String usertypeName);

  List<UsertypeEntity> findByUsertypeInfo(String usertypeInfo);

  Page<UsertypeEntity> findByUsertypeInfo(String usertypeInfo, Pageable page);

  List<UsertypeEntity> findByUsertypeInfoIgnoreCase(String usertypeInfo);

  Page<UsertypeEntity> findByUsertypeInfoIgnoreCase(String usertypeInfo, Pageable page);

  List<UsertypeEntity> findByUsertypeInfoContaining(String usertypeInfo);

  Page<UsertypeEntity> findByUsertypeInfoContaining(String usertypeInfo, Pageable page);

  List<UsertypeEntity> findByUsertypeInfoEndingWith(String usertypeInfo);

  Page<UsertypeEntity> findByUsertypeInfoEndingWith(String usertypeInfo, Pageable page);

  List<UsertypeEntity> findByUsertypeInfoStartingWith(String usertypeInfo);

  Page<UsertypeEntity> findByUsertypeInfoStartingWith(String usertypeInfo, Pageable page);

  List<UsertypeEntity> findByUsertypeInfoNotLike(String usertypeInfo);

  Page<UsertypeEntity> findByUsertypeInfoNotLike(String usertypeInfo, Pageable page);

  List<UsertypeEntity> findByUsertypeInfoLike(String usertypeInfo);

  Page<UsertypeEntity> findByUsertypeInfoLike(String usertypeInfo, Pageable page);

  List<UsertypeEntity> findByUsertypeInfoIn(Collection<String> usertypeInfos);

  List<UsertypeEntity> findByUsertypeInfoNotIn(Collection<String> usertypeInfos);

  List<UsertypeEntity> findByUsertypeInfoNull();

  List<UsertypeEntity> findByUsertypeInfoNotNull();

  List<UsertypeEntity> findByUsertypeInfoBefore(String usertypeInfo);

  List<UsertypeEntity> findByUsertypeInfoAfter(String usertypeInfo);

  List<UsertypeEntity> findByUsertypeInfoGreaterThanEqual(String usertypeInfo);

  List<UsertypeEntity> findByUsertypeInfoGreaterThan(String usertypeInfo);

  List<UsertypeEntity> findByUsertypeInfoLessThanEqual(String usertypeInfo);

  List<UsertypeEntity> findByUsertypeInfoLessThan(String usertypeInfo);

  List<UsertypeEntity> findByUsertypeCode(String usertypeCode);

  Page<UsertypeEntity> findByUsertypeCode(String usertypeCode, Pageable page);

  List<UsertypeEntity> findByUsertypeCodeIgnoreCase(String usertypeCode);

  Page<UsertypeEntity> findByUsertypeCodeIgnoreCase(String usertypeCode, Pageable page);

  List<UsertypeEntity> findByUsertypeCodeContaining(String usertypeCode);

  Page<UsertypeEntity> findByUsertypeCodeContaining(String usertypeCode, Pageable page);

  List<UsertypeEntity> findByUsertypeCodeEndingWith(String usertypeCode);

  Page<UsertypeEntity> findByUsertypeCodeEndingWith(String usertypeCode, Pageable page);

  List<UsertypeEntity> findByUsertypeCodeStartingWith(String usertypeCode);

  Page<UsertypeEntity> findByUsertypeCodeStartingWith(String usertypeCode, Pageable page);

  List<UsertypeEntity> findByUsertypeCodeNotLike(String usertypeCode);

  Page<UsertypeEntity> findByUsertypeCodeNotLike(String usertypeCode, Pageable page);

  List<UsertypeEntity> findByUsertypeCodeLike(String usertypeCode);

  Page<UsertypeEntity> findByUsertypeCodeLike(String usertypeCode, Pageable page);

  List<UsertypeEntity> findByUsertypeCodeIn(Collection<String> usertypeCodes);

  List<UsertypeEntity> findByUsertypeCodeNotIn(Collection<String> usertypeCodes);

  List<UsertypeEntity> findByUsertypeCodeNull();

  List<UsertypeEntity> findByUsertypeCodeNotNull();

  List<UsertypeEntity> findByUsertypeCodeBefore(String usertypeCode);

  List<UsertypeEntity> findByUsertypeCodeAfter(String usertypeCode);

  List<UsertypeEntity> findByUsertypeCodeGreaterThanEqual(String usertypeCode);

  List<UsertypeEntity> findByUsertypeCodeGreaterThan(String usertypeCode);

  List<UsertypeEntity> findByUsertypeCodeLessThanEqual(String usertypeCode);

  List<UsertypeEntity> findByUsertypeCodeLessThan(String usertypeCode);
}
