package it.restminer.example.api.repository;

import it.restminer.example.api.entity.RoleEntity;
import it.restminer.example.api.entity.UsertypeEntity;
import it.restminer.example.api.entity.UsertypeRoleEntity;
import java.lang.Long;
import java.util.Collection;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("usertypeRoleEntityRepository")
public interface UsertypeRoleEntityRepository extends JpaRepository<UsertypeRoleEntity, Long>, JpaSpecificationExecutor<UsertypeRoleEntity> {
  List<UsertypeRoleEntity> findById(Long id);

  Page<UsertypeRoleEntity> findById(Long id, Pageable page);

  List<UsertypeRoleEntity> findByIdIn(Collection<Long> ids);

  List<UsertypeRoleEntity> findByIdNotIn(Collection<Long> ids);

  List<UsertypeRoleEntity> findByIdNull();

  List<UsertypeRoleEntity> findByIdNotNull();

  List<UsertypeRoleEntity> findByIdBefore(Long id);

  List<UsertypeRoleEntity> findByIdAfter(Long id);

  List<UsertypeRoleEntity> findByIdGreaterThanEqual(Long id);

  List<UsertypeRoleEntity> findByIdGreaterThan(Long id);

  List<UsertypeRoleEntity> findByIdLessThanEqual(Long id);

  List<UsertypeRoleEntity> findByIdLessThan(Long id);

  List<UsertypeRoleEntity> findByIdBetween(Long idFrom, Long idTo);

  List<UsertypeRoleEntity> findByRoleEntity(RoleEntity roleEntity);

  Page<UsertypeRoleEntity> findByRoleEntity(RoleEntity roleEntity, Pageable page);

  List<UsertypeRoleEntity> findByRoleEntityIn(Collection<RoleEntity> roleEntitys);

  List<UsertypeRoleEntity> findByRoleEntityNotIn(Collection<RoleEntity> roleEntitys);

  List<UsertypeRoleEntity> findByRoleEntityNull();

  List<UsertypeRoleEntity> findByRoleEntityNotNull();

  List<UsertypeRoleEntity> findByRoleEntityBefore(RoleEntity roleEntity);

  List<UsertypeRoleEntity> findByRoleEntityAfter(RoleEntity roleEntity);

  List<UsertypeRoleEntity> findByRoleEntityGreaterThanEqual(RoleEntity roleEntity);

  List<UsertypeRoleEntity> findByRoleEntityGreaterThan(RoleEntity roleEntity);

  List<UsertypeRoleEntity> findByRoleEntityLessThanEqual(RoleEntity roleEntity);

  List<UsertypeRoleEntity> findByRoleEntityLessThan(RoleEntity roleEntity);

  List<UsertypeRoleEntity> findByRoleEntityBetween(RoleEntity roleEntityFrom,
      RoleEntity roleEntityTo);

  List<UsertypeRoleEntity> findByUsertypeEntity(UsertypeEntity usertypeEntity);

  Page<UsertypeRoleEntity> findByUsertypeEntity(UsertypeEntity usertypeEntity, Pageable page);

  List<UsertypeRoleEntity> findByUsertypeEntityIn(Collection<UsertypeEntity> usertypeEntitys);

  List<UsertypeRoleEntity> findByUsertypeEntityNotIn(Collection<UsertypeEntity> usertypeEntitys);

  List<UsertypeRoleEntity> findByUsertypeEntityNull();

  List<UsertypeRoleEntity> findByUsertypeEntityNotNull();

  List<UsertypeRoleEntity> findByUsertypeEntityBefore(UsertypeEntity usertypeEntity);

  List<UsertypeRoleEntity> findByUsertypeEntityAfter(UsertypeEntity usertypeEntity);

  List<UsertypeRoleEntity> findByUsertypeEntityGreaterThanEqual(UsertypeEntity usertypeEntity);

  List<UsertypeRoleEntity> findByUsertypeEntityGreaterThan(UsertypeEntity usertypeEntity);

  List<UsertypeRoleEntity> findByUsertypeEntityLessThanEqual(UsertypeEntity usertypeEntity);

  List<UsertypeRoleEntity> findByUsertypeEntityLessThan(UsertypeEntity usertypeEntity);

  List<UsertypeRoleEntity> findByUsertypeEntityBetween(UsertypeEntity usertypeEntityFrom,
      UsertypeEntity usertypeEntityTo);
}
