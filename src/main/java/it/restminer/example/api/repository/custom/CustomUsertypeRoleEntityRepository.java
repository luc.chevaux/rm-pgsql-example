package it.restminer.example.api.repository.custom;

import it.restminer.example.api.repository.UsertypeRoleEntityRepository;
import org.springframework.stereotype.Repository;

@Repository("customUsertypeRoleEntityRepository")
public interface CustomUsertypeRoleEntityRepository extends UsertypeRoleEntityRepository {
}
