package it.restminer.example.api.repository;

import it.restminer.example.api.entity.RoleEntity;
import java.lang.Long;
import java.lang.String;
import java.util.Collection;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("roleEntityRepository")
public interface RoleEntityRepository extends JpaRepository<RoleEntity, Long>, JpaSpecificationExecutor<RoleEntity> {
  List<RoleEntity> findByRoleId(Long roleId);

  Page<RoleEntity> findByRoleId(Long roleId, Pageable page);

  List<RoleEntity> findByRoleIdIn(Collection<Long> roleIds);

  List<RoleEntity> findByRoleIdNotIn(Collection<Long> roleIds);

  List<RoleEntity> findByRoleIdNull();

  List<RoleEntity> findByRoleIdNotNull();

  List<RoleEntity> findByRoleIdBefore(Long roleId);

  List<RoleEntity> findByRoleIdAfter(Long roleId);

  List<RoleEntity> findByRoleIdGreaterThanEqual(Long roleId);

  List<RoleEntity> findByRoleIdGreaterThan(Long roleId);

  List<RoleEntity> findByRoleIdLessThanEqual(Long roleId);

  List<RoleEntity> findByRoleIdLessThan(Long roleId);

  List<RoleEntity> findByRoleIdBetween(Long roleIdFrom, Long roleIdTo);

  List<RoleEntity> findByRoleName(String roleName);

  Page<RoleEntity> findByRoleName(String roleName, Pageable page);

  List<RoleEntity> findByRoleNameIgnoreCase(String roleName);

  Page<RoleEntity> findByRoleNameIgnoreCase(String roleName, Pageable page);

  List<RoleEntity> findByRoleNameContaining(String roleName);

  Page<RoleEntity> findByRoleNameContaining(String roleName, Pageable page);

  List<RoleEntity> findByRoleNameEndingWith(String roleName);

  Page<RoleEntity> findByRoleNameEndingWith(String roleName, Pageable page);

  List<RoleEntity> findByRoleNameStartingWith(String roleName);

  Page<RoleEntity> findByRoleNameStartingWith(String roleName, Pageable page);

  List<RoleEntity> findByRoleNameNotLike(String roleName);

  Page<RoleEntity> findByRoleNameNotLike(String roleName, Pageable page);

  List<RoleEntity> findByRoleNameLike(String roleName);

  Page<RoleEntity> findByRoleNameLike(String roleName, Pageable page);

  List<RoleEntity> findByRoleNameIn(Collection<String> roleNames);

  List<RoleEntity> findByRoleNameNotIn(Collection<String> roleNames);

  List<RoleEntity> findByRoleNameNull();

  List<RoleEntity> findByRoleNameNotNull();

  List<RoleEntity> findByRoleNameBefore(String roleName);

  List<RoleEntity> findByRoleNameAfter(String roleName);

  List<RoleEntity> findByRoleNameGreaterThanEqual(String roleName);

  List<RoleEntity> findByRoleNameGreaterThan(String roleName);

  List<RoleEntity> findByRoleNameLessThanEqual(String roleName);

  List<RoleEntity> findByRoleNameLessThan(String roleName);

  List<RoleEntity> findByRoleRights(String roleRights);

  Page<RoleEntity> findByRoleRights(String roleRights, Pageable page);

  List<RoleEntity> findByRoleRightsIgnoreCase(String roleRights);

  Page<RoleEntity> findByRoleRightsIgnoreCase(String roleRights, Pageable page);

  List<RoleEntity> findByRoleRightsContaining(String roleRights);

  Page<RoleEntity> findByRoleRightsContaining(String roleRights, Pageable page);

  List<RoleEntity> findByRoleRightsEndingWith(String roleRights);

  Page<RoleEntity> findByRoleRightsEndingWith(String roleRights, Pageable page);

  List<RoleEntity> findByRoleRightsStartingWith(String roleRights);

  Page<RoleEntity> findByRoleRightsStartingWith(String roleRights, Pageable page);

  List<RoleEntity> findByRoleRightsNotLike(String roleRights);

  Page<RoleEntity> findByRoleRightsNotLike(String roleRights, Pageable page);

  List<RoleEntity> findByRoleRightsLike(String roleRights);

  Page<RoleEntity> findByRoleRightsLike(String roleRights, Pageable page);

  List<RoleEntity> findByRoleRightsIn(Collection<String> roleRightss);

  List<RoleEntity> findByRoleRightsNotIn(Collection<String> roleRightss);

  List<RoleEntity> findByRoleRightsNull();

  List<RoleEntity> findByRoleRightsNotNull();

  List<RoleEntity> findByRoleRightsBefore(String roleRights);

  List<RoleEntity> findByRoleRightsAfter(String roleRights);

  List<RoleEntity> findByRoleRightsGreaterThanEqual(String roleRights);

  List<RoleEntity> findByRoleRightsGreaterThan(String roleRights);

  List<RoleEntity> findByRoleRightsLessThanEqual(String roleRights);

  List<RoleEntity> findByRoleRightsLessThan(String roleRights);
}
