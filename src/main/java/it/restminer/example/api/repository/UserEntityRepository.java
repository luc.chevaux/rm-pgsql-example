package it.restminer.example.api.repository;

import it.restminer.example.api.entity.UserEntity;
import it.restminer.example.api.entity.UsertypeEntity;
import java.lang.Long;
import java.lang.String;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("userEntityRepository")
public interface UserEntityRepository extends JpaRepository<UserEntity, Long>, JpaSpecificationExecutor<UserEntity> {
  List<UserEntity> findByUserId(Long userId);

  Page<UserEntity> findByUserId(Long userId, Pageable page);

  List<UserEntity> findByUserIdIn(Collection<Long> userIds);

  List<UserEntity> findByUserIdNotIn(Collection<Long> userIds);

  List<UserEntity> findByUserIdNull();

  List<UserEntity> findByUserIdNotNull();

  List<UserEntity> findByUserIdBefore(Long userId);

  List<UserEntity> findByUserIdAfter(Long userId);

  List<UserEntity> findByUserIdGreaterThanEqual(Long userId);

  List<UserEntity> findByUserIdGreaterThan(Long userId);

  List<UserEntity> findByUserIdLessThanEqual(Long userId);

  List<UserEntity> findByUserIdLessThan(Long userId);

  List<UserEntity> findByUserIdBetween(Long userIdFrom, Long userIdTo);

  List<UserEntity> findByUsertypeEntity(UsertypeEntity usertypeEntity);

  Page<UserEntity> findByUsertypeEntity(UsertypeEntity usertypeEntity, Pageable page);

  List<UserEntity> findByUsertypeEntityIn(Collection<UsertypeEntity> usertypeEntitys);

  List<UserEntity> findByUsertypeEntityNotIn(Collection<UsertypeEntity> usertypeEntitys);

  List<UserEntity> findByUsertypeEntityNull();

  List<UserEntity> findByUsertypeEntityNotNull();

  List<UserEntity> findByUsertypeEntityBefore(UsertypeEntity usertypeEntity);

  List<UserEntity> findByUsertypeEntityAfter(UsertypeEntity usertypeEntity);

  List<UserEntity> findByUsertypeEntityGreaterThanEqual(UsertypeEntity usertypeEntity);

  List<UserEntity> findByUsertypeEntityGreaterThan(UsertypeEntity usertypeEntity);

  List<UserEntity> findByUsertypeEntityLessThanEqual(UsertypeEntity usertypeEntity);

  List<UserEntity> findByUsertypeEntityLessThan(UsertypeEntity usertypeEntity);

  List<UserEntity> findByUsertypeEntityBetween(UsertypeEntity usertypeEntityFrom,
      UsertypeEntity usertypeEntityTo);

  List<UserEntity> findByUserNickname(String userNickname);

  Page<UserEntity> findByUserNickname(String userNickname, Pageable page);

  List<UserEntity> findByUserNicknameIgnoreCase(String userNickname);

  Page<UserEntity> findByUserNicknameIgnoreCase(String userNickname, Pageable page);

  List<UserEntity> findByUserNicknameContaining(String userNickname);

  Page<UserEntity> findByUserNicknameContaining(String userNickname, Pageable page);

  List<UserEntity> findByUserNicknameEndingWith(String userNickname);

  Page<UserEntity> findByUserNicknameEndingWith(String userNickname, Pageable page);

  List<UserEntity> findByUserNicknameStartingWith(String userNickname);

  Page<UserEntity> findByUserNicknameStartingWith(String userNickname, Pageable page);

  List<UserEntity> findByUserNicknameNotLike(String userNickname);

  Page<UserEntity> findByUserNicknameNotLike(String userNickname, Pageable page);

  List<UserEntity> findByUserNicknameLike(String userNickname);

  Page<UserEntity> findByUserNicknameLike(String userNickname, Pageable page);

  List<UserEntity> findByUserNicknameIn(Collection<String> userNicknames);

  List<UserEntity> findByUserNicknameNotIn(Collection<String> userNicknames);

  List<UserEntity> findByUserNicknameNull();

  List<UserEntity> findByUserNicknameNotNull();

  List<UserEntity> findByUserNicknameBefore(String userNickname);

  List<UserEntity> findByUserNicknameAfter(String userNickname);

  List<UserEntity> findByUserNicknameGreaterThanEqual(String userNickname);

  List<UserEntity> findByUserNicknameGreaterThan(String userNickname);

  List<UserEntity> findByUserNicknameLessThanEqual(String userNickname);

  List<UserEntity> findByUserNicknameLessThan(String userNickname);

  List<UserEntity> findByUserBirthdate(Date userBirthdate);

  Page<UserEntity> findByUserBirthdate(Date userBirthdate, Pageable page);

  List<UserEntity> findByUserBirthdateIn(Collection<Date> userBirthdates);

  List<UserEntity> findByUserBirthdateNotIn(Collection<Date> userBirthdates);

  List<UserEntity> findByUserBirthdateNull();

  List<UserEntity> findByUserBirthdateNotNull();

  List<UserEntity> findByUserBirthdateBefore(Date userBirthdate);

  List<UserEntity> findByUserBirthdateAfter(Date userBirthdate);

  List<UserEntity> findByUserBirthdateGreaterThanEqual(Date userBirthdate);

  List<UserEntity> findByUserBirthdateGreaterThan(Date userBirthdate);

  List<UserEntity> findByUserBirthdateLessThanEqual(Date userBirthdate);

  List<UserEntity> findByUserBirthdateLessThan(Date userBirthdate);

  List<UserEntity> findByUserBirthdateBetween(Date userBirthdateFrom, Date userBirthdateTo);

  List<UserEntity> findByUserLastlogin(Date userLastlogin);

  Page<UserEntity> findByUserLastlogin(Date userLastlogin, Pageable page);

  List<UserEntity> findByUserLastloginIn(Collection<Date> userLastlogins);

  List<UserEntity> findByUserLastloginNotIn(Collection<Date> userLastlogins);

  List<UserEntity> findByUserLastloginNull();

  List<UserEntity> findByUserLastloginNotNull();

  List<UserEntity> findByUserLastloginBefore(Date userLastlogin);

  List<UserEntity> findByUserLastloginAfter(Date userLastlogin);

  List<UserEntity> findByUserLastloginGreaterThanEqual(Date userLastlogin);

  List<UserEntity> findByUserLastloginGreaterThan(Date userLastlogin);

  List<UserEntity> findByUserLastloginLessThanEqual(Date userLastlogin);

  List<UserEntity> findByUserLastloginLessThan(Date userLastlogin);

  List<UserEntity> findByUserLastloginBetween(Date userLastloginFrom, Date userLastloginTo);
}
