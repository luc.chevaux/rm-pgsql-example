package it.restminer.example.api.repository.custom;

import it.restminer.example.api.repository.PostEntityRepository;
import org.springframework.stereotype.Repository;

@Repository("customPostEntityRepository")
public interface CustomPostEntityRepository extends PostEntityRepository {
}
