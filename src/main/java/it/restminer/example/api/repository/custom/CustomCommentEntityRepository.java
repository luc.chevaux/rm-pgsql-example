package it.restminer.example.api.repository.custom;

import it.restminer.example.api.repository.CommentEntityRepository;
import org.springframework.stereotype.Repository;

@Repository("customCommentEntityRepository")
public interface CustomCommentEntityRepository extends CommentEntityRepository {
}
