package it.restminer.example.api.repository.custom;

import it.restminer.example.api.repository.UserEntityRepository;
import org.springframework.stereotype.Repository;

@Repository("customUserEntityRepository")
public interface CustomUserEntityRepository extends UserEntityRepository {
}
