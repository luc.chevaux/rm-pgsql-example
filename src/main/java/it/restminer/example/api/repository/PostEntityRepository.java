package it.restminer.example.api.repository;

import it.restminer.example.api.entity.PostEntity;
import it.restminer.example.api.entity.UserEntity;
import java.lang.Long;
import java.lang.String;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository("postEntityRepository")
public interface PostEntityRepository extends JpaRepository<PostEntity, Long>, JpaSpecificationExecutor<PostEntity> {
  List<PostEntity> findByPostId(Long postId);

  Page<PostEntity> findByPostId(Long postId, Pageable page);

  List<PostEntity> findByPostIdIn(Collection<Long> postIds);

  List<PostEntity> findByPostIdNotIn(Collection<Long> postIds);

  List<PostEntity> findByPostIdNull();

  List<PostEntity> findByPostIdNotNull();

  List<PostEntity> findByPostIdBefore(Long postId);

  List<PostEntity> findByPostIdAfter(Long postId);

  List<PostEntity> findByPostIdGreaterThanEqual(Long postId);

  List<PostEntity> findByPostIdGreaterThan(Long postId);

  List<PostEntity> findByPostIdLessThanEqual(Long postId);

  List<PostEntity> findByPostIdLessThan(Long postId);

  List<PostEntity> findByPostIdBetween(Long postIdFrom, Long postIdTo);

  List<PostEntity> findByUserEntity(UserEntity userEntity);

  Page<PostEntity> findByUserEntity(UserEntity userEntity, Pageable page);

  List<PostEntity> findByUserEntityIn(Collection<UserEntity> userEntitys);

  List<PostEntity> findByUserEntityNotIn(Collection<UserEntity> userEntitys);

  List<PostEntity> findByUserEntityNull();

  List<PostEntity> findByUserEntityNotNull();

  List<PostEntity> findByUserEntityBefore(UserEntity userEntity);

  List<PostEntity> findByUserEntityAfter(UserEntity userEntity);

  List<PostEntity> findByUserEntityGreaterThanEqual(UserEntity userEntity);

  List<PostEntity> findByUserEntityGreaterThan(UserEntity userEntity);

  List<PostEntity> findByUserEntityLessThanEqual(UserEntity userEntity);

  List<PostEntity> findByUserEntityLessThan(UserEntity userEntity);

  List<PostEntity> findByUserEntityBetween(UserEntity userEntityFrom, UserEntity userEntityTo);

  List<PostEntity> findByPostName(String postName);

  Page<PostEntity> findByPostName(String postName, Pageable page);

  List<PostEntity> findByPostNameIgnoreCase(String postName);

  Page<PostEntity> findByPostNameIgnoreCase(String postName, Pageable page);

  List<PostEntity> findByPostNameContaining(String postName);

  Page<PostEntity> findByPostNameContaining(String postName, Pageable page);

  List<PostEntity> findByPostNameEndingWith(String postName);

  Page<PostEntity> findByPostNameEndingWith(String postName, Pageable page);

  List<PostEntity> findByPostNameStartingWith(String postName);

  Page<PostEntity> findByPostNameStartingWith(String postName, Pageable page);

  List<PostEntity> findByPostNameNotLike(String postName);

  Page<PostEntity> findByPostNameNotLike(String postName, Pageable page);

  List<PostEntity> findByPostNameLike(String postName);

  Page<PostEntity> findByPostNameLike(String postName, Pageable page);

  List<PostEntity> findByPostNameIn(Collection<String> postNames);

  List<PostEntity> findByPostNameNotIn(Collection<String> postNames);

  List<PostEntity> findByPostNameNull();

  List<PostEntity> findByPostNameNotNull();

  List<PostEntity> findByPostNameBefore(String postName);

  List<PostEntity> findByPostNameAfter(String postName);

  List<PostEntity> findByPostNameGreaterThanEqual(String postName);

  List<PostEntity> findByPostNameGreaterThan(String postName);

  List<PostEntity> findByPostNameLessThanEqual(String postName);

  List<PostEntity> findByPostNameLessThan(String postName);

  List<PostEntity> findByPostTitle(String postTitle);

  Page<PostEntity> findByPostTitle(String postTitle, Pageable page);

  List<PostEntity> findByPostTitleIgnoreCase(String postTitle);

  Page<PostEntity> findByPostTitleIgnoreCase(String postTitle, Pageable page);

  List<PostEntity> findByPostTitleContaining(String postTitle);

  Page<PostEntity> findByPostTitleContaining(String postTitle, Pageable page);

  List<PostEntity> findByPostTitleEndingWith(String postTitle);

  Page<PostEntity> findByPostTitleEndingWith(String postTitle, Pageable page);

  List<PostEntity> findByPostTitleStartingWith(String postTitle);

  Page<PostEntity> findByPostTitleStartingWith(String postTitle, Pageable page);

  List<PostEntity> findByPostTitleNotLike(String postTitle);

  Page<PostEntity> findByPostTitleNotLike(String postTitle, Pageable page);

  List<PostEntity> findByPostTitleLike(String postTitle);

  Page<PostEntity> findByPostTitleLike(String postTitle, Pageable page);

  List<PostEntity> findByPostTitleIn(Collection<String> postTitles);

  List<PostEntity> findByPostTitleNotIn(Collection<String> postTitles);

  List<PostEntity> findByPostTitleNull();

  List<PostEntity> findByPostTitleNotNull();

  List<PostEntity> findByPostTitleBefore(String postTitle);

  List<PostEntity> findByPostTitleAfter(String postTitle);

  List<PostEntity> findByPostTitleGreaterThanEqual(String postTitle);

  List<PostEntity> findByPostTitleGreaterThan(String postTitle);

  List<PostEntity> findByPostTitleLessThanEqual(String postTitle);

  List<PostEntity> findByPostTitleLessThan(String postTitle);

  List<PostEntity> findByPostContent(String postContent);

  Page<PostEntity> findByPostContent(String postContent, Pageable page);

  List<PostEntity> findByPostContentIgnoreCase(String postContent);

  Page<PostEntity> findByPostContentIgnoreCase(String postContent, Pageable page);

  List<PostEntity> findByPostContentContaining(String postContent);

  Page<PostEntity> findByPostContentContaining(String postContent, Pageable page);

  List<PostEntity> findByPostContentEndingWith(String postContent);

  Page<PostEntity> findByPostContentEndingWith(String postContent, Pageable page);

  List<PostEntity> findByPostContentStartingWith(String postContent);

  Page<PostEntity> findByPostContentStartingWith(String postContent, Pageable page);

  List<PostEntity> findByPostContentNotLike(String postContent);

  Page<PostEntity> findByPostContentNotLike(String postContent, Pageable page);

  List<PostEntity> findByPostContentLike(String postContent);

  Page<PostEntity> findByPostContentLike(String postContent, Pageable page);

  List<PostEntity> findByPostContentIn(Collection<String> postContents);

  List<PostEntity> findByPostContentNotIn(Collection<String> postContents);

  List<PostEntity> findByPostContentNull();

  List<PostEntity> findByPostContentNotNull();

  List<PostEntity> findByPostContentBefore(String postContent);

  List<PostEntity> findByPostContentAfter(String postContent);

  List<PostEntity> findByPostContentGreaterThanEqual(String postContent);

  List<PostEntity> findByPostContentGreaterThan(String postContent);

  List<PostEntity> findByPostContentLessThanEqual(String postContent);

  List<PostEntity> findByPostContentLessThan(String postContent);

  List<PostEntity> findByPostImage(String postImage);

  Page<PostEntity> findByPostImage(String postImage, Pageable page);

  List<PostEntity> findByPostImageIgnoreCase(String postImage);

  Page<PostEntity> findByPostImageIgnoreCase(String postImage, Pageable page);

  List<PostEntity> findByPostImageContaining(String postImage);

  Page<PostEntity> findByPostImageContaining(String postImage, Pageable page);

  List<PostEntity> findByPostImageEndingWith(String postImage);

  Page<PostEntity> findByPostImageEndingWith(String postImage, Pageable page);

  List<PostEntity> findByPostImageStartingWith(String postImage);

  Page<PostEntity> findByPostImageStartingWith(String postImage, Pageable page);

  List<PostEntity> findByPostImageNotLike(String postImage);

  Page<PostEntity> findByPostImageNotLike(String postImage, Pageable page);

  List<PostEntity> findByPostImageLike(String postImage);

  Page<PostEntity> findByPostImageLike(String postImage, Pageable page);

  List<PostEntity> findByPostImageIn(Collection<String> postImages);

  List<PostEntity> findByPostImageNotIn(Collection<String> postImages);

  List<PostEntity> findByPostImageNull();

  List<PostEntity> findByPostImageNotNull();

  List<PostEntity> findByPostImageBefore(String postImage);

  List<PostEntity> findByPostImageAfter(String postImage);

  List<PostEntity> findByPostImageGreaterThanEqual(String postImage);

  List<PostEntity> findByPostImageGreaterThan(String postImage);

  List<PostEntity> findByPostImageLessThanEqual(String postImage);

  List<PostEntity> findByPostImageLessThan(String postImage);

  List<PostEntity> findByPostDate(Date postDate);

  Page<PostEntity> findByPostDate(Date postDate, Pageable page);

  List<PostEntity> findByPostDateIn(Collection<Date> postDates);

  List<PostEntity> findByPostDateNotIn(Collection<Date> postDates);

  List<PostEntity> findByPostDateNull();

  List<PostEntity> findByPostDateNotNull();

  List<PostEntity> findByPostDateBefore(Date postDate);

  List<PostEntity> findByPostDateAfter(Date postDate);

  List<PostEntity> findByPostDateGreaterThanEqual(Date postDate);

  List<PostEntity> findByPostDateGreaterThan(Date postDate);

  List<PostEntity> findByPostDateLessThanEqual(Date postDate);

  List<PostEntity> findByPostDateLessThan(Date postDate);

  List<PostEntity> findByPostDateBetween(Date postDateFrom, Date postDateTo);

  List<PostEntity> findByPostLikes(Long postLikes);

  Page<PostEntity> findByPostLikes(Long postLikes, Pageable page);

  List<PostEntity> findByPostLikesIn(Collection<Long> postLikess);

  List<PostEntity> findByPostLikesNotIn(Collection<Long> postLikess);

  List<PostEntity> findByPostLikesNull();

  List<PostEntity> findByPostLikesNotNull();

  List<PostEntity> findByPostLikesBefore(Long postLikes);

  List<PostEntity> findByPostLikesAfter(Long postLikes);

  List<PostEntity> findByPostLikesGreaterThanEqual(Long postLikes);

  List<PostEntity> findByPostLikesGreaterThan(Long postLikes);

  List<PostEntity> findByPostLikesLessThanEqual(Long postLikes);

  List<PostEntity> findByPostLikesLessThan(Long postLikes);

  List<PostEntity> findByPostLikesBetween(Long postLikesFrom, Long postLikesTo);
}
