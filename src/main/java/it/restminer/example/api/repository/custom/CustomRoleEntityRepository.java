package it.restminer.example.api.repository.custom;

import it.restminer.example.api.repository.RoleEntityRepository;
import org.springframework.stereotype.Repository;

@Repository("customRoleEntityRepository")
public interface CustomRoleEntityRepository extends RoleEntityRepository {
}
