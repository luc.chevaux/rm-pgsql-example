package it.restminer.example.api.config;

import java.lang.Override;
import java.lang.String;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@PropertySource("classpath:application.properties")
@Configuration
@EnableWebMvc
@ComponentScan({"${example.base.package}","${restminer.base.package.common}"})
public class WebConfig extends WebMvcConfigurerAdapter {
  @Value("${jwt.token.header}")
  private String tokenHeader;

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    CorsRegistration registration = registry.addMapping("/**");
    registration.allowedOrigins("*");
    registration.allowedMethods(HttpMethod.GET.name(), HttpMethod.POST.name(), HttpMethod.PUT.name(), HttpMethod.DELETE.name(), HttpMethod.OPTIONS.name());
    registration.allowedHeaders("Origin" , "X-Requested-With", "Content-Type", "Accept", tokenHeader);
    registration.allowCredentials(false);
    registration.maxAge(3600);
  }
}
