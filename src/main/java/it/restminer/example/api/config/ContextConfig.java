package it.restminer.example.api.config;

import it.restminer.common.mapper.FieldMapper;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@PropertySource("classpath:application.properties")
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    entityManagerFactoryRef = "exampleEntityManagerFactory",
    transactionManagerRef = "exampleTransactionManager",
    basePackages = {"${example.jpa.repository.package}"}
)
public class ContextConfig {
  @Value("${example.jpa.entity.package}")
  private String entityPackage;

  @Autowired
  private Environment environment;

  @Bean
  public LocalContainerEntityManagerFactoryBean exampleEntityManagerFactory() {
    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(exampleDataSource());
    em.setPackagesToScan(new String[] { entityPackage });
    JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    em.setJpaVendorAdapter(vendorAdapter);
    em.setJpaProperties(exampleHibernateProperties());
    em.setPersistenceUnitName("examplePU");
    return em;
  }

  public Properties exampleHibernateProperties() {
    Properties properties = new Properties();
    properties.put("hibernate.dialect", environment.getRequiredProperty("example.hibernate.dialect"));
    properties.put("hibernate.show_sql", environment.getRequiredProperty("example.hibernate.show_sql"));
    properties.put("hibernate.format_sql", environment.getRequiredProperty("example.hibernate.format_sql"));
    properties.put("hibernate.temp.use_jdbc_metadata_defaults", environment.getRequiredProperty("example.hibernate.use_jdbc_metadata_defaults"));
    properties.put("hibernate.default_schema", "example");
    return properties;
  }

  @Bean
  public DriverManagerDataSource exampleDataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(environment.getRequiredProperty("example.jdbc.driverClassName"));
    dataSource.setUrl(environment.getRequiredProperty("example.jdbc.url"));
    dataSource.setUsername(environment.getRequiredProperty("example.jdbc.username"));
    dataSource.setPassword(environment.getRequiredProperty("example.jdbc.password"));
    return dataSource;
  }

  @Bean
  @Qualifier("exampleTM")
  public JpaTransactionManager exampleTransactionManager() {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(exampleEntityManagerFactory().getObject());
    return transactionManager;
  }

  @Bean(
      name = "mapper"
  )
  public DozerBeanMapper configDozerBeanMapper() {
    DozerBeanMapper mapper = new DozerBeanMapper();
    List<String> mappingFileUrls = new ArrayList<String>();
    mappingFileUrls.add("dozer-bean-mappings.xml");
    mapper.setMappingFiles(mappingFileUrls);
    mapper.setCustomFieldMapper(new FieldMapper());
    return mapper;
  }
}
