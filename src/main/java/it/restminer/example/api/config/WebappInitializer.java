package it.restminer.example.api.config;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class WebappInitializer implements WebApplicationInitializer {
  public void onStartup(ServletContext container) {
    AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
    ctx.register(WebConfig.class, ContextConfig.class);
    ctx.setServletContext(container);
    ServletRegistration.Dynamic servlet = container.addServlet("dispatcher", new DispatcherServlet(ctx));
    Map<String, String> arg0 = new HashMap<String, String>();
    arg0.put("dispatchOptionsRequest", "true");
    servlet.setInitParameters(arg0);
    servlet.setLoadOnStartup(1);
    servlet.addMapping("/");
  }
}
