package it.restminer.example.api.controller;

import it.restminer.common.controller.AbstractController;
import it.restminer.common.dto.CreateResponseDTO;
import it.restminer.common.dto.DeleteResponseDTO;
import it.restminer.common.dto.PagedDTO;
import it.restminer.common.dto.UpdateResponseDTO;
import it.restminer.common.exception.RecordNotFoundException;
import it.restminer.common.logging.annotation.Loggable;
import it.restminer.common.processor.CreateRequestProcessor;
import it.restminer.common.processor.CriteriaRequestProcessor;
import it.restminer.common.processor.DeleteRequestProcessor;
import it.restminer.common.processor.UpdateRequestProcessor;
import it.restminer.common.util.Validator;
import it.restminer.example.api.entity.CommentEntity;
import it.restminer.example.api.entity.dto.CommentEntityDTO;
import it.restminer.example.api.repository.CommentEntityRepository;
import it.restminer.example.api.repository.custom.CustomCommentEntityRepository;
import java.lang.Exception;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RestController
@RequestMapping("Comment")
@PropertySource("classpath:rest.platform.properties")
public class CommentEntityController extends AbstractController {
  private static final String entityId = "commentId";

  private static final String entityName = "CommentEntity";

  Logger logger = Logger.getLogger(this.getClass());

  @Autowired
  @Qualifier("commentEntityRepository")
  private CommentEntityRepository repo;

  @Autowired
  @Qualifier("customCommentEntityRepository")
  private CustomCommentEntityRepository customRepo;

  @Autowired
  private DozerBeanMapper mapper;

  @Autowired
  private Environment env;

  @PersistenceContext(
      unitName = "examplePU"
  )
  private EntityManager em;

  @Autowired
  private Validator validator;

  @Override
  protected CreateRequestProcessor<CommentEntityRepository, CommentEntity, CommentEntityDTO> getCreator() {
    return new CreateRequestProcessor<CommentEntityRepository, CommentEntity, CommentEntityDTO>(repo, mapper, CommentEntity.class, entityName, env, em);
  }

  @Override
  protected UpdateRequestProcessor<CommentEntityRepository, CommentEntity, CommentEntityDTO> getUpdater() {
    return new UpdateRequestProcessor<CommentEntityRepository, CommentEntity, CommentEntityDTO>(repo, mapper, entityName, entityId, env, em);
  }

  @Override
  protected CriteriaRequestProcessor<CommentEntityRepository, CommentEntity, CommentEntityDTO> getProcessor() {
    return new CriteriaRequestProcessor<CommentEntityRepository, CommentEntity, CommentEntityDTO>(repo, mapper, CommentEntityDTO.class, entityName, env);
  }

  @Override
  protected DeleteRequestProcessor<CommentEntityRepository, CommentEntity, CommentEntityDTO> getEraser() {
    return new DeleteRequestProcessor<CommentEntityRepository, CommentEntity, CommentEntityDTO>(repo, mapper, entityName, entityId, env, em);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/{id}"
  )
  @Transactional(
      transactionManager = "exampleTM",
      readOnly = true
  )
  @Loggable
  public @ResponseBody CommentEntityDTO get(@PathVariable Long id,
      @RequestParam(defaultValue = "", required = false) String showCollection) throws Exception {
    logger.info(MessageFormat.format(env.getProperty("echo.api.crud.logs.getting"), entityName, entityId, id.toString()));
    CriteriaRequestProcessor<CommentEntityRepository, CommentEntity, CommentEntityDTO> processor = getProcessor();
    processor.setCriteria(entityId + "::" + id);
    Map<String,String> sorts = new HashMap<>();
    sorts.put(entityId, "1");
    processor.setPageCriteria(sorts, 1, 1);
    processor.setShowCollection(showCollection);
    CommentEntityDTO entity = processor.process().getElements().get(0);
    if (entity == null) {
      logger.warn(MessageFormat.format(env.getProperty("echo.api.crud.search.noresult"), entityName, entityId, id.toString()));
      throw new RecordNotFoundException(entityName, entityId, id.toString());
    }
    logger.info(MessageFormat.format(env.getProperty("echo.api.crud.logs.returning.response"), entityName, entityId, id.toString()));
    return entity;
  }

  @RequestMapping(
      method = RequestMethod.POST
  )
  @Transactional("exampleTM")
  @Loggable
  public @ResponseBody CreateResponseDTO<CommentEntityDTO> add(@RequestBody CommentEntityDTO item,
      HttpServletRequest request) throws Exception {
    logger.info(env.getProperty("echo.api.crud.logs.validating"));
    CreateRequestProcessor<CommentEntityRepository, CommentEntity, CommentEntityDTO> creator = getCreator();
    creator.setCreatedUser(getLoggedUser(request));
    creator.setDto(item);
    logger.info(MessageFormat.format(env.getProperty("echo.api.crud.logs.adding"), entityName));
    return creator.process();
  }

  @RequestMapping(
      method = RequestMethod.PUT
  )
  @Transactional("exampleTM")
  @Loggable
  public @ResponseBody UpdateResponseDTO<CommentEntityDTO> update(@RequestBody CommentEntityDTO item,
      HttpServletRequest request) throws Exception {
    logger.info(env.getProperty("echo.api.crud.logs.validating"));
    validator.validateDTOIdd(item, entityName);
    UpdateRequestProcessor<CommentEntityRepository, CommentEntity, CommentEntityDTO> updater = getUpdater();
    updater.setSourceDto(item);
    updater.setUpdatedUser(getLoggedUser(request));
    logger.info(MessageFormat.format(env.getProperty("echo.api.crud.logs.updating"), entityName, entityId, item.getIdd().toString()));
    return updater.process();
  }

  @RequestMapping(
      method = RequestMethod.DELETE
  )
  @Transactional("exampleTM")
  @Loggable
  public @ResponseBody DeleteResponseDTO<CommentEntityDTO> delete(@RequestBody CommentEntityDTO item,
      HttpServletRequest request) throws Exception {
    logger.info(env.getProperty("echo.api.crud.logs.validating"));
    validator.validateDTOIdd(item, entityName);
    DeleteRequestProcessor<CommentEntityRepository, CommentEntity, CommentEntityDTO> eraser = getEraser();
    eraser.setSourceDto(item);
    eraser.setUpdatedUser(getLoggedUser(request));
    logger.info(MessageFormat.format(env.getProperty("echo.api.crud.logs.updating"), entityName, entityId, item.getIdd().toString()));
    return eraser.process();
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = ""
  )
  @Transactional(
      transactionManager = "exampleTM",
      readOnly = true
  )
  @Loggable
  public @ResponseBody PagedDTO<CommentEntityDTO> getByCriteria(@RequestParam(defaultValue = "null", required = false) String criteria,
      @RequestParam(defaultValue = "1", required = false) int page,
      @RequestParam(defaultValue = "10", required = false) int size,
      @RequestParam(defaultValue = "", required = false) String showCollection,
      HttpServletRequest request) throws Exception {
    logger.info(env.getProperty("echo.api.crud.logs.validating"));
    Map<String,String> sorts = extractSort(request, "sorts");
    validator.validateSortField(sorts, it.restminer.example.api.entity.CommentEntity.class, entityName);
    CriteriaRequestProcessor<CommentEntityRepository, CommentEntity, CommentEntityDTO> processor = getProcessor();
    processor.setCriteria(criteria);
    processor.setPageCriteria(sorts, page, size);
    processor.setShowCollection(showCollection);
    logger.info(MessageFormat.format(env.getProperty("echo.api.crud.logs.getting.with.criteria"), entityName, criteria));
    return processor.process();
  }

  @Override
  public String getEntityId() {
    return entityId;
  }
}
