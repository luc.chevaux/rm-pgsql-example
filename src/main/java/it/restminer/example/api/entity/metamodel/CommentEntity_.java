package it.restminer.example.api.entity.metamodel;
import it.restminer.example.api.entity.*;


import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CommentEntity.class)
public abstract class CommentEntity_ {

	public static volatile SingularAttribute<CommentEntity, String> commentAuthor;
	public static volatile SingularAttribute<CommentEntity, Date> commentDate;
	public static volatile SingularAttribute<CommentEntity, Long> commentId;
	public static volatile SingularAttribute<CommentEntity, PostEntity> postEntity;
	public static volatile SingularAttribute<CommentEntity, String> commentText;

}

