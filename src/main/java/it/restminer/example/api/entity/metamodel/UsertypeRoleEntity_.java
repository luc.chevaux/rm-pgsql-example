package it.restminer.example.api.entity.metamodel;
import it.restminer.example.api.entity.*;


import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UsertypeRoleEntity.class)
public abstract class UsertypeRoleEntity_ {

	public static volatile SingularAttribute<UsertypeRoleEntity, UsertypeEntity> usertypeEntity;
	public static volatile SingularAttribute<UsertypeRoleEntity, Long> id;
	public static volatile SingularAttribute<UsertypeRoleEntity, RoleEntity> roleEntity;

}

