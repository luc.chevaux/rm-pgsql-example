package it.restminer.example.api.entity;


import it.restminer.common.entity.AbstractEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;




@Table(name = "ex_usertype_role")
@Entity
@DynamicInsert
@DynamicUpdate
public class UsertypeRoleEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = -6726141210764657113L;

    private Long id;

    private RoleEntity roleEntity;

    private UsertypeEntity usertypeEntity;

    public UsertypeRoleEntity() {
    }

    public UsertypeRoleEntity(Long id) {
        this.id = id;
    }

    public UsertypeRoleEntity(Long id, RoleEntity roleEntity, UsertypeEntity usertypeEntity) {
        this.id = id;
        this.roleEntity = roleEntity;
        this.usertypeEntity = usertypeEntity;
    }

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id")
    public RoleEntity getRoleEntity() {
        return this.roleEntity;
    }

    public void setRoleEntity(RoleEntity roleEntity) {
        this.roleEntity = roleEntity;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usertype_id")
    public UsertypeEntity getUsertypeEntity() {
        return this.usertypeEntity;
    }

    public void setUsertypeEntity(UsertypeEntity usertypeEntity) {
        this.usertypeEntity = usertypeEntity;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((id) == null ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if ((this) == obj)
            return true;

        if (obj == null)
            return false;

        if ((getClass()) != (obj.getClass()))
            return false;

        UsertypeRoleEntity other = (UsertypeRoleEntity) obj;
        if ((id) == null) {
            if ((other.id) != null)
                return false;

        }else
            if (!(id.equals(other.id)))
                return false;


        return true;
    }

    @Override
    @Transient
    public Serializable getAbstractEntityId() {
        return id;
    }
}

