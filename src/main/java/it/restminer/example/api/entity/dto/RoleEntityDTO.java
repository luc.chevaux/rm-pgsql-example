package it.restminer.example.api.entity.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import it.restminer.common.annotation.Idd;
import it.restminer.common.dto.AbstractDTO;
import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties("idd")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "roleId"
)
public class RoleEntityDTO extends AbstractDTO implements Serializable {
  private static final long serialVersionUID = -2170616570907709906l;

  private Long roleId;

  private String roleName;

  private String roleRights;

  private Set<UsertypeRoleEntityDTO> usertypeRoleEntities;

  @Idd
  public Long getRoleId() {
    return roleId;
  }

  public void setRoleId(Long roleId) {
    this.roleId = roleId;
  }

  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }

  public String getRoleRights() {
    return roleRights;
  }

  public void setRoleRights(String roleRights) {
    this.roleRights = roleRights;
  }

  public Set<UsertypeRoleEntityDTO> getUsertypeRoleEntities() {
    return usertypeRoleEntities;
  }

  public void setUsertypeRoleEntities(Set<UsertypeRoleEntityDTO> usertypeRoleEntities) {
    this.usertypeRoleEntities = usertypeRoleEntities;
  }

  public Long getIdd() {
    return getRoleId();
  }
}
