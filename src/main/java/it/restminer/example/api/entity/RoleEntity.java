package it.restminer.example.api.entity;


import it.restminer.common.entity.AbstractEntity;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;




@Table(name = "ex_role")
@Entity
@DynamicInsert
@DynamicUpdate
public class RoleEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = -2170616570907709906L;

    private Long roleId;

    private String roleName;

    private String roleRights;

    private Set<UsertypeRoleEntity> usertypeRoleEntities = new HashSet<UsertypeRoleEntity>(0);

    public RoleEntity() {
    }

    public RoleEntity(Long roleId) {
        this.roleId = roleId;
    }

    public RoleEntity(Long roleId, String roleName, String roleRights, Set<UsertypeRoleEntity> usertypeRoleEntities) {
        this.roleId = roleId;
        this.roleName = roleName;
        this.roleRights = roleRights;
        this.usertypeRoleEntities = usertypeRoleEntities;
    }

    @Id
    @Column(name = "role_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getRoleId() {
        return this.roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    @Column(name = "role_name")
    public String getRoleName() {
        return this.roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Column(name = "role_rights")
    public String getRoleRights() {
        return this.roleRights;
    }

    public void setRoleRights(String roleRights) {
        this.roleRights = roleRights;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "roleEntity")
    public Set<UsertypeRoleEntity> getUsertypeRoleEntities() {
        return this.usertypeRoleEntities;
    }

    public void setUsertypeRoleEntities(Set<UsertypeRoleEntity> usertypeRoleEntities) {
        this.usertypeRoleEntities = usertypeRoleEntities;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((roleId) == null ? 0 : roleId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if ((this) == obj)
            return true;

        if (obj == null)
            return false;

        if ((getClass()) != (obj.getClass()))
            return false;

        RoleEntity other = (RoleEntity) obj;
        if ((roleId) == null) {
            if ((other.roleId) != null)
                return false;

        }else
            if (!(roleId.equals(other.roleId)))
                return false;


        return true;
    }

    @Override
    @Transient
    public Serializable getAbstractEntityId() {
        return roleId;
    }
}

