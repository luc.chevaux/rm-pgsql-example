package it.restminer.example.api.entity.metamodel;
import it.restminer.example.api.entity.*;


import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserEntity.class)
public abstract class UserEntity_ {

	public static volatile SetAttribute<UserEntity, PostEntity> postEntities;
	public static volatile SingularAttribute<UserEntity, String> userNickname;
	public static volatile SingularAttribute<UserEntity, UsertypeEntity> usertypeEntity;
	public static volatile SingularAttribute<UserEntity, Long> userId;
	public static volatile SingularAttribute<UserEntity, Date> userBirthdate;
	public static volatile SingularAttribute<UserEntity, Date> userLastlogin;

}

