package it.restminer.example.api.entity.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import it.restminer.common.annotation.Idd;
import it.restminer.common.dto.AbstractDTO;
import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import java.util.Date;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties("idd")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "postId"
)
public class PostEntityDTO extends AbstractDTO implements Serializable {
  private static final long serialVersionUID = 4284978389028851265l;

  private Long postId;

  private UserEntityDTO userEntity;

  private String postName;

  private String postTitle;

  private String postContent;

  private String postImage;

  private Date postDate;

  private Long postLikes;

  private Set<CommentEntityDTO> commentEntities;

  @Idd
  public Long getPostId() {
    return postId;
  }

  public void setPostId(Long postId) {
    this.postId = postId;
  }

  public UserEntityDTO getUserEntity() {
    return userEntity;
  }

  public void setUserEntity(UserEntityDTO userEntity) {
    this.userEntity = userEntity;
  }

  public String getPostName() {
    return postName;
  }

  public void setPostName(String postName) {
    this.postName = postName;
  }

  public String getPostTitle() {
    return postTitle;
  }

  public void setPostTitle(String postTitle) {
    this.postTitle = postTitle;
  }

  public String getPostContent() {
    return postContent;
  }

  public void setPostContent(String postContent) {
    this.postContent = postContent;
  }

  public String getPostImage() {
    return postImage;
  }

  public void setPostImage(String postImage) {
    this.postImage = postImage;
  }

  public Date getPostDate() {
    return postDate;
  }

  public void setPostDate(Date postDate) {
    this.postDate = postDate;
  }

  public Long getPostLikes() {
    return postLikes;
  }

  public void setPostLikes(Long postLikes) {
    this.postLikes = postLikes;
  }

  public Set<CommentEntityDTO> getCommentEntities() {
    return commentEntities;
  }

  public void setCommentEntities(Set<CommentEntityDTO> commentEntities) {
    this.commentEntities = commentEntities;
  }

  public Long getIdd() {
    return getPostId();
  }
}
