package it.restminer.example.api.entity;


import it.restminer.common.entity.AbstractEntity;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;




@Table(name = "ex_user")
@Entity
@DynamicInsert
@DynamicUpdate
public class UserEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = -8747680933868197309L;

    private Long userId;

    private UsertypeEntity usertypeEntity;

    private String userNickname;

    private Date userBirthdate;

    private Date userLastlogin;

    private Set<PostEntity> postEntities = new HashSet<PostEntity>(0);

    public UserEntity() {
    }

    public UserEntity(Long userId) {
        this.userId = userId;
    }

    public UserEntity(Long userId, UsertypeEntity usertypeEntity, String userNickname, Date userBirthdate, Date userLastlogin, Set<PostEntity> postEntities) {
        this.userId = userId;
        this.usertypeEntity = usertypeEntity;
        this.userNickname = userNickname;
        this.userBirthdate = userBirthdate;
        this.userLastlogin = userLastlogin;
        this.postEntities = postEntities;
    }

    @Id
    @Column(name = "user_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usertype_id")
    public UsertypeEntity getUsertypeEntity() {
        return this.usertypeEntity;
    }

    public void setUsertypeEntity(UsertypeEntity usertypeEntity) {
        this.usertypeEntity = usertypeEntity;
    }

    @Column(name = "user_nickname")
    public String getUserNickname() {
        return this.userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    @Column(name = "user_birthdate", length = 13)
    public Date getUserBirthdate() {
        return this.userBirthdate;
    }

    public void setUserBirthdate(Date userBirthdate) {
        this.userBirthdate = userBirthdate;
    }

    @Column(name = "user_lastlogin", length = 29)
    public Date getUserLastlogin() {
        return this.userLastlogin;
    }

    public void setUserLastlogin(Date userLastlogin) {
        this.userLastlogin = userLastlogin;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userEntity")
    public Set<PostEntity> getPostEntities() {
        return this.postEntities;
    }

    public void setPostEntities(Set<PostEntity> postEntities) {
        this.postEntities = postEntities;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((userId) == null ? 0 : userId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if ((this) == obj)
            return true;

        if (obj == null)
            return false;

        if ((getClass()) != (obj.getClass()))
            return false;

        UserEntity other = (UserEntity) obj;
        if ((userId) == null) {
            if ((other.userId) != null)
                return false;

        }else
            if (!(userId.equals(other.userId)))
                return false;


        return true;
    }

    @Override
    @Transient
    public Serializable getAbstractEntityId() {
        return userId;
    }
}

