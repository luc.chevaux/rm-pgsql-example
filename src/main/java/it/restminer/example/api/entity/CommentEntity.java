package it.restminer.example.api.entity;


import it.restminer.common.entity.AbstractEntity;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;




@Table(name = "ex_comment")
@Entity
@DynamicInsert
@DynamicUpdate
public class CommentEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = -4440810813269718092L;

    private Long commentId;

    private PostEntity postEntity;

    private String commentText;

    private String commentAuthor;

    private Date commentDate;

    public CommentEntity() {
    }

    public CommentEntity(Long commentId) {
        this.commentId = commentId;
    }

    public CommentEntity(Long commentId, PostEntity postEntity, String commentText, String commentAuthor, Date commentDate) {
        this.commentId = commentId;
        this.postEntity = postEntity;
        this.commentText = commentText;
        this.commentAuthor = commentAuthor;
        this.commentDate = commentDate;
    }

    @Id
    @Column(name = "comment_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getCommentId() {
        return this.commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id")
    public PostEntity getPostEntity() {
        return this.postEntity;
    }

    public void setPostEntity(PostEntity postEntity) {
        this.postEntity = postEntity;
    }

    @Column(name = "comment_text")
    public String getCommentText() {
        return this.commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    @Column(name = "comment_author")
    public String getCommentAuthor() {
        return this.commentAuthor;
    }

    public void setCommentAuthor(String commentAuthor) {
        this.commentAuthor = commentAuthor;
    }

    @Column(name = "comment_date", length = 29)
    public Date getCommentDate() {
        return this.commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((commentId) == null ? 0 : commentId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if ((this) == obj)
            return true;

        if (obj == null)
            return false;

        if ((getClass()) != (obj.getClass()))
            return false;

        CommentEntity other = (CommentEntity) obj;
        if ((commentId) == null) {
            if ((other.commentId) != null)
                return false;

        }else
            if (!(commentId.equals(other.commentId)))
                return false;


        return true;
    }

    @Override
    @Transient
    public Serializable getAbstractEntityId() {
        return commentId;
    }
}

