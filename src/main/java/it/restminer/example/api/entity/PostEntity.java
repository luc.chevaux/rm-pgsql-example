package it.restminer.example.api.entity;


import it.restminer.common.entity.AbstractEntity;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;




@Table(name = "ex_post")
@Entity
@DynamicInsert
@DynamicUpdate
public class PostEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = 4284978389028851265L;

    private Long postId;

    private UserEntity userEntity;

    private String postName;

    private String postTitle;

    private String postContent;

    private String postImage;

    private Date postDate;

    private Long postLikes;

    private Set<CommentEntity> commentEntities = new HashSet<CommentEntity>(0);

    public PostEntity() {
    }

    public PostEntity(Long postId) {
        this.postId = postId;
    }

    public PostEntity(Long postId, UserEntity userEntity, String postName, String postTitle, String postContent, String postImage, Date postDate, Long postLikes, Set<CommentEntity> commentEntities) {
        this.postId = postId;
        this.userEntity = userEntity;
        this.postName = postName;
        this.postTitle = postTitle;
        this.postContent = postContent;
        this.postImage = postImage;
        this.postDate = postDate;
        this.postLikes = postLikes;
        this.commentEntities = commentEntities;
    }

    @Id
    @Column(name = "post_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getPostId() {
        return this.postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    public UserEntity getUserEntity() {
        return this.userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    @Column(name = "post_name")
    public String getPostName() {
        return this.postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    @Column(name = "post_title")
    public String getPostTitle() {
        return this.postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    @Column(name = "post_content")
    public String getPostContent() {
        return this.postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    @Column(name = "post_image")
    public String getPostImage() {
        return this.postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    @Column(name = "post_date", length = 29)
    public Date getPostDate() {
        return this.postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    @Column(name = "post_likes")
    public Long getPostLikes() {
        return this.postLikes;
    }

    public void setPostLikes(Long postLikes) {
        this.postLikes = postLikes;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "postEntity")
    public Set<CommentEntity> getCommentEntities() {
        return this.commentEntities;
    }

    public void setCommentEntities(Set<CommentEntity> commentEntities) {
        this.commentEntities = commentEntities;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((postId) == null ? 0 : postId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if ((this) == obj)
            return true;

        if (obj == null)
            return false;

        if ((getClass()) != (obj.getClass()))
            return false;

        PostEntity other = (PostEntity) obj;
        if ((postId) == null) {
            if ((other.postId) != null)
                return false;

        }else
            if (!(postId.equals(other.postId)))
                return false;


        return true;
    }

    @Override
    @Transient
    public Serializable getAbstractEntityId() {
        return postId;
    }
}

