package it.restminer.example.api.entity.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import it.restminer.common.annotation.Idd;
import it.restminer.common.dto.AbstractDTO;
import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties("idd")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "usertypeId"
)
public class UsertypeEntityDTO extends AbstractDTO implements Serializable {
  private static final long serialVersionUID = -5307959527368771764l;

  private Long usertypeId;

  private String usertypeName;

  private String usertypeInfo;

  private String usertypeCode;

  private Set<UserEntityDTO> userEntities;

  private Set<UsertypeRoleEntityDTO> usertypeRoleEntities;

  @Idd
  public Long getUsertypeId() {
    return usertypeId;
  }

  public void setUsertypeId(Long usertypeId) {
    this.usertypeId = usertypeId;
  }

  public String getUsertypeName() {
    return usertypeName;
  }

  public void setUsertypeName(String usertypeName) {
    this.usertypeName = usertypeName;
  }

  public String getUsertypeInfo() {
    return usertypeInfo;
  }

  public void setUsertypeInfo(String usertypeInfo) {
    this.usertypeInfo = usertypeInfo;
  }

  public String getUsertypeCode() {
    return usertypeCode;
  }

  public void setUsertypeCode(String usertypeCode) {
    this.usertypeCode = usertypeCode;
  }

  public Set<UserEntityDTO> getUserEntities() {
    return userEntities;
  }

  public void setUserEntities(Set<UserEntityDTO> userEntities) {
    this.userEntities = userEntities;
  }

  public Set<UsertypeRoleEntityDTO> getUsertypeRoleEntities() {
    return usertypeRoleEntities;
  }

  public void setUsertypeRoleEntities(Set<UsertypeRoleEntityDTO> usertypeRoleEntities) {
    this.usertypeRoleEntities = usertypeRoleEntities;
  }

  public Long getIdd() {
    return getUsertypeId();
  }
}
