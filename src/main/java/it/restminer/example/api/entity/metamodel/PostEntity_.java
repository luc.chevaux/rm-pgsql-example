package it.restminer.example.api.entity.metamodel;
import it.restminer.example.api.entity.*;


import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PostEntity.class)
public abstract class PostEntity_ {

	public static volatile SingularAttribute<PostEntity, UserEntity> userEntity;
	public static volatile SingularAttribute<PostEntity, String> postImage;
	public static volatile SingularAttribute<PostEntity, String> postContent;
	public static volatile SingularAttribute<PostEntity, String> postName;
	public static volatile SingularAttribute<PostEntity, Date> postDate;
	public static volatile SingularAttribute<PostEntity, String> postTitle;
	public static volatile SingularAttribute<PostEntity, Long> postId;
	public static volatile SetAttribute<PostEntity, CommentEntity> commentEntities;
	public static volatile SingularAttribute<PostEntity, Long> postLikes;

}

