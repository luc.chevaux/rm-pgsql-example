package it.restminer.example.api.entity.metamodel;
import it.restminer.example.api.entity.*;


import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UsertypeEntity.class)
public abstract class UsertypeEntity_ {

	public static volatile SetAttribute<UsertypeEntity, UserEntity> userEntities;
	public static volatile SingularAttribute<UsertypeEntity, String> usertypeInfo;
	public static volatile SingularAttribute<UsertypeEntity, String> usertypeCode;
	public static volatile SetAttribute<UsertypeEntity, UsertypeRoleEntity> usertypeRoleEntities;
	public static volatile SingularAttribute<UsertypeEntity, Long> usertypeId;
	public static volatile SingularAttribute<UsertypeEntity, String> usertypeName;

}

