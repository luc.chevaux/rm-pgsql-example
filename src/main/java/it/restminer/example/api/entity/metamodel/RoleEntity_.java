package it.restminer.example.api.entity.metamodel;
import it.restminer.example.api.entity.*;


import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RoleEntity.class)
public abstract class RoleEntity_ {

	public static volatile SingularAttribute<RoleEntity, Long> roleId;
	public static volatile SingularAttribute<RoleEntity, String> roleRights;
	public static volatile SingularAttribute<RoleEntity, String> roleName;
	public static volatile SetAttribute<RoleEntity, UsertypeRoleEntity> usertypeRoleEntities;

}

