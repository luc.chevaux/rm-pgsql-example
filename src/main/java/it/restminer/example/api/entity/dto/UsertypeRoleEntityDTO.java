package it.restminer.example.api.entity.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import it.restminer.common.annotation.Idd;
import it.restminer.common.dto.AbstractDTO;
import java.io.Serializable;
import java.lang.Long;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties("idd")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "id"
)
public class UsertypeRoleEntityDTO extends AbstractDTO implements Serializable {
  private static final long serialVersionUID = -6726141210764657113l;

  private Long id;

  private RoleEntityDTO roleEntity;

  private UsertypeEntityDTO usertypeEntity;

  @Idd
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public RoleEntityDTO getRoleEntity() {
    return roleEntity;
  }

  public void setRoleEntity(RoleEntityDTO roleEntity) {
    this.roleEntity = roleEntity;
  }

  public UsertypeEntityDTO getUsertypeEntity() {
    return usertypeEntity;
  }

  public void setUsertypeEntity(UsertypeEntityDTO usertypeEntity) {
    this.usertypeEntity = usertypeEntity;
  }

  public Long getIdd() {
    return getId();
  }
}
