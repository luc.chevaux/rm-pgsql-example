package it.restminer.example.api.entity.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import it.restminer.common.annotation.Idd;
import it.restminer.common.dto.AbstractDTO;
import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties("idd")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "commentId"
)
public class CommentEntityDTO extends AbstractDTO implements Serializable {
  private static final long serialVersionUID = -4440810813269718092l;

  private Long commentId;

  private PostEntityDTO postEntity;

  private String commentText;

  private String commentAuthor;

  private Date commentDate;

  @Idd
  public Long getCommentId() {
    return commentId;
  }

  public void setCommentId(Long commentId) {
    this.commentId = commentId;
  }

  public PostEntityDTO getPostEntity() {
    return postEntity;
  }

  public void setPostEntity(PostEntityDTO postEntity) {
    this.postEntity = postEntity;
  }

  public String getCommentText() {
    return commentText;
  }

  public void setCommentText(String commentText) {
    this.commentText = commentText;
  }

  public String getCommentAuthor() {
    return commentAuthor;
  }

  public void setCommentAuthor(String commentAuthor) {
    this.commentAuthor = commentAuthor;
  }

  public Date getCommentDate() {
    return commentDate;
  }

  public void setCommentDate(Date commentDate) {
    this.commentDate = commentDate;
  }

  public Long getIdd() {
    return getCommentId();
  }
}
