package it.restminer.example.api.entity;


import it.restminer.common.entity.AbstractEntity;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;




@Table(name = "ex_usertype")
@Entity
@DynamicInsert
@DynamicUpdate
public class UsertypeEntity extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = -5307959527368771764L;

    private Long usertypeId;

    private String usertypeName;

    private String usertypeInfo;

    private String usertypeCode;

    private Set<UserEntity> userEntities = new HashSet<UserEntity>(0);

    private Set<UsertypeRoleEntity> usertypeRoleEntities = new HashSet<UsertypeRoleEntity>(0);

    public UsertypeEntity() {
    }

    public UsertypeEntity(Long usertypeId) {
        this.usertypeId = usertypeId;
    }

    public UsertypeEntity(Long usertypeId, String usertypeName, String usertypeInfo, String usertypeCode, Set<UserEntity> userEntities, Set<UsertypeRoleEntity> usertypeRoleEntities) {
        this.usertypeId = usertypeId;
        this.usertypeName = usertypeName;
        this.usertypeInfo = usertypeInfo;
        this.usertypeCode = usertypeCode;
        this.userEntities = userEntities;
        this.usertypeRoleEntities = usertypeRoleEntities;
    }

    @Id
    @Column(name = "usertype_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getUsertypeId() {
        return this.usertypeId;
    }

    public void setUsertypeId(Long usertypeId) {
        this.usertypeId = usertypeId;
    }

    @Column(name = "usertype_name")
    public String getUsertypeName() {
        return this.usertypeName;
    }

    public void setUsertypeName(String usertypeName) {
        this.usertypeName = usertypeName;
    }

    @Column(name = "usertype_info")
    public String getUsertypeInfo() {
        return this.usertypeInfo;
    }

    public void setUsertypeInfo(String usertypeInfo) {
        this.usertypeInfo = usertypeInfo;
    }

    @Column(name = "usertype_code", length = 2)
    public String getUsertypeCode() {
        return this.usertypeCode;
    }

    public void setUsertypeCode(String usertypeCode) {
        this.usertypeCode = usertypeCode;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usertypeEntity")
    public Set<UserEntity> getUserEntities() {
        return this.userEntities;
    }

    public void setUserEntities(Set<UserEntity> userEntities) {
        this.userEntities = userEntities;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usertypeEntity")
    public Set<UsertypeRoleEntity> getUsertypeRoleEntities() {
        return this.usertypeRoleEntities;
    }

    public void setUsertypeRoleEntities(Set<UsertypeRoleEntity> usertypeRoleEntities) {
        this.usertypeRoleEntities = usertypeRoleEntities;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((usertypeId) == null ? 0 : usertypeId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if ((this) == obj)
            return true;

        if (obj == null)
            return false;

        if ((getClass()) != (obj.getClass()))
            return false;

        UsertypeEntity other = (UsertypeEntity) obj;
        if ((usertypeId) == null) {
            if ((other.usertypeId) != null)
                return false;

        }else
            if (!(usertypeId.equals(other.usertypeId)))
                return false;


        return true;
    }

    @Override
    @Transient
    public Serializable getAbstractEntityId() {
        return usertypeId;
    }
}

