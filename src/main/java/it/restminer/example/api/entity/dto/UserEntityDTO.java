package it.restminer.example.api.entity.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import it.restminer.common.annotation.Idd;
import it.restminer.common.dto.AbstractDTO;
import java.io.Serializable;
import java.lang.Long;
import java.lang.String;
import java.util.Date;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties("idd")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "userId"
)
public class UserEntityDTO extends AbstractDTO implements Serializable {
  private static final long serialVersionUID = -8747680933868197309l;

  private Long userId;

  private UsertypeEntityDTO usertypeEntity;

  private String userNickname;

  private Date userBirthdate;

  private Date userLastlogin;

  private Set<PostEntityDTO> postEntities;

  @Idd
  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public UsertypeEntityDTO getUsertypeEntity() {
    return usertypeEntity;
  }

  public void setUsertypeEntity(UsertypeEntityDTO usertypeEntity) {
    this.usertypeEntity = usertypeEntity;
  }

  public String getUserNickname() {
    return userNickname;
  }

  public void setUserNickname(String userNickname) {
    this.userNickname = userNickname;
  }

  public Date getUserBirthdate() {
    return userBirthdate;
  }

  public void setUserBirthdate(Date userBirthdate) {
    this.userBirthdate = userBirthdate;
  }

  public Date getUserLastlogin() {
    return userLastlogin;
  }

  public void setUserLastlogin(Date userLastlogin) {
    this.userLastlogin = userLastlogin;
  }

  public Set<PostEntityDTO> getPostEntities() {
    return postEntities;
  }

  public void setPostEntities(Set<PostEntityDTO> postEntities) {
    this.postEntities = postEntities;
  }

  public Long getIdd() {
    return getUserId();
  }
}
