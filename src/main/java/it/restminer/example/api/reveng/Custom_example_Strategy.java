package it.restminer.example.api.reveng;

import java.lang.Override;
import java.lang.String;
import org.hibernate.cfg.reveng.DelegatingReverseEngineeringStrategy;
import org.hibernate.cfg.reveng.ReverseEngineeringStrategy;
import org.hibernate.cfg.reveng.TableIdentifier;

public class Custom_example_Strategy extends DelegatingReverseEngineeringStrategy {
  public Custom_example_Strategy(ReverseEngineeringStrategy delegate) {
    super(delegate);
  }

  @Override
  public String columnToPropertyName(TableIdentifier table, String column) {
    return Character.toLowerCase(super.columnToPropertyName(table, column).charAt(0)) + super.columnToPropertyName(table, column).substring(1);
  }

  @Override
  public String tableToClassName(TableIdentifier tableIdentifier) {
    String tableNameWithPackage = super.tableToClassName(tableIdentifier);
    int lastDotPos = tableNameWithPackage.lastIndexOf(".");
    String tableOriginalClassName =  tableNameWithPackage.substring(++lastDotPos, tableNameWithPackage.length());
    String replaced = tableOriginalClassName.replace("Ex", "");
    String compositeName = tableNameWithPackage.substring(0, lastDotPos) + "" + replaced + "Entity";
    return compositeName;
  }
}
